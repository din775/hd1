@echo off

:start
cls
echo.
echo.
echo Bem Vindo ao Menu Tijolinho!
echo ================
echo Digite 1. Para que eu lhe mostre quais modulos python voce ja tem instalado
echo Digite 2. Para que eu instale algum modulo novo (CUIDADO)
echo Digite 3. Para desinstalar algum modulo (MAIS CUIDADO AINDA!)
echo.
 

set python_ver=37
 
set /p x=Escolha:
IF '%x%' == '1' GOTO NUM_1
IF '%x%' == '2' GOTO NUM_2
IF '%x%' == '3' GOTO NUM_3
GOTO start
 
:NUM_1
cd \
cd \python%python_ver%\Scripts\
pip freeze
pause
exit
 
:NUM_2
echo  Coloque o nome de algum modulo para que eu procure
set INPUT=
set /P INPUT=Diga:%=%
 
cd \
cd \python%python_ver%\Scripts\
pip install %INPUT% --cert"inserir caminho do certificado aqui!"
 
pause
exit
 
:NUM_3
echo  Entre o nome do modulo que eu irei DESINSTALAR
set INPUT=
set /P INPUT=Diga:%=%
 
cd \
cd \python%python_ver%\Scripts\
pip uninstall %INPUT%
 
pause
exit